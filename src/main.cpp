#include <mystic-window-library/mystic-window-library.h>
#include "color.h"
#include "game.h"

int main(int argc, char *argv[])
{
    gen_colors();
    game_init();
    application = mystic_init_application("Snake", window_width, window_height, true, 70, black);
    return mystic_main_loop(application, game_event_loop);
}
