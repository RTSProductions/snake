#include <stdint.h>
#include <iostream>

uint32_t black;
uint32_t white;
uint32_t green;
uint32_t red;

void gen_colors()
{
    black = mystic_rgb_values_to_uint32(0, 0, 0);
    white = mystic_rgb_values_to_uint32(255, 255, 255);
    green = mystic_rgb_values_to_uint32(0, 255, 0); 
    red = mystic_rgb_values_to_uint32(255, 0, 0); 
}
