#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <iostream>

mystic_application_t application;

signed short int window_width = 950;
signed short int window_height = 600;
#include "classes.h"
bool displaying_start_menu;
int body_size = 0;
int frame_skip = 0;
Vector2 up;
Vector2 down;
Vector2 left;
Vector2 right;

Vector2 Direction;

Player player;
Food food;
Snake_Body bodies[1];
void game_init()
{
    
    std::cout << "Game: initiating game\n";
    player.init(window_width / 2, window_height / 2, 50, 50);
    food.init(40, 40);
    bodies[1].init(100, 100, 50, 50);
    
}

void game_over()
{
    delete[] bodies;
    game_init();
}

void render_game()
{
    mystic_draw_rectangle(application, 0, 0, window_width, window_height, black);
    food.render();
    player.render();
    bodies[1].init(player.position.x, player.position.y, 50, 50);
    for (int i = body_size; i > 1; i--)
    {
    	int j = i - 1;
    	if (frame_skip >= 2)
    	{
    		bodies[i].position.init(bodies[j].position.x + Direction.x, bodies[j].position.y + Direction.y);
    	}
    	bodies[i].scale.init(50, 50);
    	bodies[i].render();
    	if (player.position.x < bodies[i].position.x + bodies[i].scale.x &&
                player.position.x + player.scale.x > bodies[i].position.x &&
                player.position.y < bodies[i].position.y + bodies[i].scale.y &&
                player.position.y + player.scale.y > bodies[i].position.y)
                {
                	game_over();
                }
    }
    frame_skip++;
    if (frame_skip > 2)
    {
    	frame_skip = 0;
    }
    
}


void game_main_loop(mystic_event_t event)
{
    render_game();
    return;
}

int game_event_loop(mystic_event_t event)
{
    if (event.event_type == MYSTIC_EVENT_WINDOW_EXPOSE)
    {
        game_main_loop(event);
        return MYSTIC_CONTINUE_LOOP;
    }
    if (event.event_type == MYSTIC_EVENT_KEYBOARD_KEY_PRESS)
    {
    	if (event.key_name == "w")
    	{
    		player.position.y -= player.speed;
    		Direction.init(0, 60);
    	}
    	else if (event.key_name == "s")
    	{
    		player.position.y += player.speed;
    		Direction.init(0, -60);
    	}
    	else if (event.key_name == "d")
    	{
    		player.position.x += player.speed;
    		Direction.init(-60, 0);
    	}
    	else if (event.key_name == "a")
    	{
    		player.position.x -= player.speed;
    		Direction.init(60, 0);
    	}
    	
    	if (player.position.x > window_width)
    	{
    		player.position.x = 0;
    	}
    	else if (player.position.x < 0)
    	{
    		player.position.x = window_width;
    	}
    	if (player.position.y > window_height)
    	{
    		player.position.y = 0;
    	}
    	else if (player.position.y < 0)
    	{
    		player.position.y = window_height;
    	}
    	
    	
    	if (player.position.x < food.position.x + food.scale.x &&
                player.position.x + player.scale.x > food.position.x &&
                player.position.y < food.position.y + food.scale.y &&
                player.position.y + player.scale.y > food.position.y)
    	{
    		Snake_Body New_Snake_Bodies[body_size + 1];
    		body_size++;
    		for (int i = 0; i < body_size; i++)
    		{
    				if (i > body_size - 1)
    				{
    					Snake_Body body = New_Snake_Bodies[i];
    					Vector2 dir;
    					dir.init(New_Snake_Bodies[i - 1].position.x + Direction.x, New_Snake_Bodies[i - 1].position.y + Direction.y);
    					New_Snake_Bodies[i].position = dir;
    					body.init(10, 10, 50, 50);
    				}
    				else
    				{
    					New_Snake_Bodies[i] = bodies[i];
    				}
    		}
    		memcpy(bodies, New_Snake_Bodies, sizeof(New_Snake_Bodies));
    		food.init(40, 40);
    		
    	}
    	
    	render_game();
    }
    return MYSTIC_CONTINUE_LOOP;
}
