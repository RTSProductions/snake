#include <time.h>
#include <stdlib.h>

class Vector2 
{
	public:
	int x;
	int y;
	
	void init (int X, int Y)
	{
	    x = X;
	    y = Y;
	    
	}
};

class Player
{
	public:
		int speed = 10;
		Vector2 position;
		Vector2 scale;
		
        	void init(int X_Pos, int Y_Pos, int X_Scale, int Y_Scale)
        	{
        		position.init(X_Pos, Y_Pos);
        		scale.init(X_Scale, Y_Scale);
        	}
        	void render()
        	{
            		mystic_draw_rectangle(application, position.x, position.y, scale.x, scale.y, green);
        	}
};

class Snake_Body 
{
	public:
		int speed = 10;
		Vector2 position;
		Vector2 scale;
		
        	void init(int X_Pos, int Y_Pos, int X_Scale, int Y_Scale)
        	{
        		position.init(X_Pos, Y_Pos);
        		scale.init(X_Scale, Y_Scale);
        	}
        	void render()
        	{
            		mystic_draw_rectangle(application, position.x, position.y, scale.x, scale.y, green);
        	}
};

class Food
{
	public: 
		Vector2 position;
		Vector2 scale;
		
        	void init(int X_Scale, int Y_Scale)
        	{
        	        srand(time(NULL));
        		scale.init(X_Scale, Y_Scale);
        		position.y = rand()%(((window_height -10)+1));
        		position.x = rand()%(((window_width -10)+1));
        	}
        	void render()
        	{
            		mystic_draw_rectangle(application, position.x, position.y, scale.x, scale.y, red);
        	}
};


