build: /usr/include/mystic-window-library/mystic-window-library.h /usr/lib/libmysticwindowlibrary.so
	mkdir ./build
	g++ ./src/main.cpp -lmysticwindowlibrary -lxcb -o ./build/snake.bin
clean: build/
	rm -r ./build
run: /usr/include/mystic-window-library/mystic-window-library.h /usr/lib/libmysticwindowlibrary.so
	make clean
	make build
	./build/snake.bin
